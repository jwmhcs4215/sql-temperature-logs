# SQL Temperature Logs

This example shows how to read a collection of data from a JSON file into a Spark DataFrame.
Additionally, it also shows how to read data from a txt file and mapping it properly into a Spark DataFrame.

Various examples of querying the imported data via DataFrame api as well as SQL queries as shown.

Finally, saving the results of a query to a parquet file on disk is also demonstrated.
Additionally, saving a subset of results to a csv file on disk is also demonstrated.