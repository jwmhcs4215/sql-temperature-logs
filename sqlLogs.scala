// the following 2 lines are not required if ran in spark-shell (spark session and spark context are already create)
//import org.apache.spark.sql.SparkSession
//val spark = SparkSession.builder.appName("SQL Transaction").getOrCreate()

import spark.implicits._

// load a json file from the same directory into a DataFrame
val logJsonDF = spark.read.json("statusLog.json")

// we can then directly perform data-processing via DataFrame api
// e.g. Find all unique temperature values
println("\nAll unique temperature values logged:")
logJsonDF.select("Temperature").distinct.show()

// alternatively, we can create a DataFrame from a txt file
// first, we create a case class to represent each entry in the data
case class LogEntry(Date: String, Time: String, Temperature: Int, Status: String)

// next, split each line using "," as separator, and map the split attributes according to the class we created
// finally, store it as a DataFrame
val logTxtDF = sc.textFile("statusLog.txt").
  map(_.split(",")).
  map(attributes => LogEntry(attributes(0), attributes(1).trim, attributes(2).trim.toInt, attributes(3).trim)).
  toDF()

// we can then perform data-processing as usual
// e.g. Find the number of times a temperature higher than 80 was logged
println("\nNumber of times a temperature higher than 80 was reached:")
logTxtDF.filter($"Temperature" > 80).count()

// we can also use SQL queries
// first, we create a SQL temporary view from the DataFrame, from which we can perform SQL queries
logTxtDF.createOrReplaceTempView("logs")

// we can also assign the resultant DataFrame as a new value
// e.g. Find the date and time whenever temperatures reached critical levels
val criticalDateTimeResults = spark.sql("SELECT Date as Critical_Date, Time as Critical_Time FROM logs WHERE Status LIKE '%critical%'")

// we can directly access columns of result rows via index
val combinedResults = criticalDateTimeResults.map(row => "Date: " + row(0) + ", Time: " + row(1))

// by default spark-shell truncates row values if they are too long, pass in false to prevent truncation
println("\nCombined Date & Time when critical temperatures were reached:")
combinedResults.show(false)

// here, we save the result DataFrame into .parquet format on disk
// this saves the file into a folder named "transactionResults-json", and rerunning this code overwrites any previous results
combinedResults.
  write.format("parquet").
  mode("overwrite").
  save("criticalTempsResults-parquet")

// alternatively, we can also save into .csv format on disk
// we can also manipulate the DataFrame right before we save, e.g. here we only save the Critical_Date column
criticalDateTimeResults.
  select("Critical_Date").
  write.format("csv").
  mode("overwrite").
  option("header", "true").
  save("criticalTempsResults-csv")